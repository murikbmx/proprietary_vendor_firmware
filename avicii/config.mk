FIRMWARE_IMAGES := \
    abl \
    aop \
    bluetooth \
    devcfg \
    dsp \
    featenabler \
    hyp \
    imagefv \
    keymaster \
    logo \
    modem \
    qupfw \
    storsec \
    tz \
    uefisecapp \
    xbl \
    xbl_config

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)
